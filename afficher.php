<?php
session_start();
require_once 'include/head.php';
require_once 'include/connexion_bdd.php';

if (!empty($_POST['idart'])) {
	$req = $pdo->prepare('
		INSERT INTO commentaire (com_pseudo, com_message, com_date, com_art_id)
		VALUES (:pseudo, :message, NOW(), :idart)
		');

	$req->execute([
		'pseudo'=> $_POST['pseudo'],
		'message'=> $_POST['message'],
		'idart'=> $_POST['idart']
	]);

}

$req = $pdo->prepare('
	SELECT * FROM article
	WHERE art_id=:id
	');

$req->execute([
	'id'=>$_GET['id']
]);
$article = $req->fetch(PDO::FETCH_ASSOC);


$comreq = $pdo->prepare('
		SELECT * FROM commentaire
		WHERE com_art_id=:id
		');

$comreq->execute([
	'id'=>$_GET['id']
	]);
$commentaires = $comreq->fetchAll(PDO::FETCH_ASSOC);
?>

<a href="index.php">Retour à l'accueil</a><br>
<h1><?=$article['art_titre']?></h1>
<p><?=$article['art_contenu']?></p>
<p><?='Dernière modification : '.date('d/m/Y H:i:s', strtotime($article['art_datemodif']))?></p>

<form action="afficher.php?id=<?=$article['art_id']?>" method="post">
	<label for="commentaire">Commentaire</label><br>
	<input type="text" id="pseudo" name="pseudo" placeholder="votre pseudo" required><br>
	<textarea name="message" id="commentaire" cols="100" rows="5" placeholder="Des commentaires?" required></textarea><br>
	<input type="hidden" name="idart" value="<?=$article['art_id']?>">
	<input type="submit" value="Valider">
</form>

<?php
foreach ($commentaires as $commentaire) {
?>

<p><?=$commentaire['com_message']?></p>
<p><?='par '.$commentaire['com_pseudo'].' le '.date('d/m/Y à H:i:s', strtotime($commentaire['com_date']))?></p>
<a href="supprimer_com.php?art_id=<?=$article['art_id'].'&id='.$commentaire['com_id']?>" onclick="return confirm('Êtes-vous certain de vouloir supprimer ce commentaire?')">Supprimer</a>

<?php
}
require_once 'include/foot.php';
?>