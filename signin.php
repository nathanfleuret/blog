<?php
session_start();
require_once 'include/head.php';
require_once 'include/connexion_bdd.php';

if (!empty($_POST)) {

	$req = $pdo->prepare('
		INSERT INTO utilisateur (uti_nom, uti_prenom, uti_email, uti_login, uti_mdp)
		VALUES (:nom, :prenom, :email, :login, :mdp)
		');

	$req->execute([
		'nom'=> $_POST['nom'],
		'prenom'=> $_POST['prenom'],
		'email'=> $_POST['email'],
		'login'=> $_POST['login'],
		'mdp'=> password_hash($_POST['mdp'], PASSWORD_DEFAULT)
	]);

	header('location:index.php');
}
?>

<h1>Inscription</h1>
<form action="signin.php" method="post">
	<input type="text" id="nom" name="nom" placeholder="Saisir votre nom" required><br>
	<input type="text" id="prenom" name="prenom" placeholder="Saisir votre prenom" required><br>
	<input type="email" id="email" name="email" placeholder="Saisir votre email" required><br>
	<input type="text" id="login" name="login" placeholder="Saisir votre login" required><br>
	<input type="password" id="mdp" name="mdp" placeholder="Saisir votre mot de passe" required><br>
	<input type="submit" value="S'inscrire">
</form>
<a href="index.php">Retour à l'accueil</a>

<?php
require_once 'include/foot.php';
?>