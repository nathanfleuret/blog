<?php
session_start();
require_once 'include/head.php';
require_once 'include/connexion_bdd.php';

$req = $pdo->query('
	SELECT * FROM article
	');

$allResults = $req->fetchAll(PDO::FETCH_ASSOC);
?>


<h1>Les articles</h1>	

<a href="creer.php">Ajouter un nouvel article</a>
<table border="1px">
	<th>Titre</th>
	<th>Date de création</th>
	<th>Voir l'article</th>
	<th>Supprimer l'article</th>

	<?php
	foreach ($allResults as $article) {
	?>
	<tr>
		<td><a href="afficher.php?id=<?=$article['art_id']?>"><?=$article['art_titre']?></a></td>
		<td><?=date_format(DateTime::createFromFormat('Y-m-d H:i:s', $article['art_datecrea']),'d/m/Y H:i:s')?></td>
		<td><a href="modifier.php?id=<?=$article['art_id']?>">Modifier l'article</a></td>
		<td><a href="supprimer.php?id=<?=$article['art_id']?>" onclick="return confirm('Êtes-vous certain de vouloir supprimer cet article?')">Supprimer l'article</a></td>
	</tr>	

	<?php
	}
	?>

</table>

<?php
require_once 'include/foot.php';
?>