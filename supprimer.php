<?php
require_once 'include/verification.php';
require_once 'include/connexion_bdd.php';

$req = $pdo->prepare('
	DELETE FROM article
	WHERE art_id=:id
	');

$req->execute([
	'id' => $_GET['id']
]);

header('location:index.php');
?>