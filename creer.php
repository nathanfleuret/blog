<?php
require_once 'include/verification.php';
require_once 'include/head.php';
require_once 'include/connexion_bdd.php';

if (!empty($_POST)) {

	$req = $pdo->prepare('
		INSERT INTO article (art_titre, art_contenu, art_datecrea, art_datemodif, art_uti_id)
		VALUES (:titre, :contenu, NOW(), NOW(), 1)
		');

	$req->execute([
		'titre'=> $_POST['titre'],
		'contenu'=> $_POST['contenu']
	]);
	header('location:index.php');
}

?>
<h1>Nouvel article</h1>
<form action="creer.php" method="post">
	<input type="text" id="titre" name="titre" placeholder="Saisir le titre de l'article" required><br>
	<textarea rows="5" cols="100" id="contenu" name="contenu" placeholder="Ecrire votre article ici" required></textarea>
	<input type="submit" value="Publier">
</form>
<a href="index.php">Retour à l'accueil</a>

<?php
require_once 'include/foot.php';
?>