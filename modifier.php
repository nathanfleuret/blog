<?php
require_once 'include/verification.php';
require_once 'include/head.php';
require_once 'include/connexion_bdd.php';

if (!empty($_POST)) {
	$req = $pdo->prepare('
		UPDATE article
		SET art_titre=:titre, art_contenu=:contenu, art_datemodif=:datemodif
		WHERE art_id=:id
	');

	$req->execute([
		'titre'=> $_POST['titre'],
		'contenu'=> $_POST['contenu'],
		'datemodif'=> date("Y-m-d H:i:s"),
		'id'=> $_POST['id']
	]);

	header('location:index.php');

} else {
	$req = $pdo->prepare('
		SELECT * FROM article
		WHERE art_id=:id
	');

	$req->execute([
		'id'=>$_GET['id']
	]);

	$article = $req->fetch(PDO::FETCH_ASSOC);
}

?>

<h1>Modifier l'article</h1>

<form action="modifier.php" method="post">
	<input type="text" id="titre" name="titre" value="<?=$article['art_titre']?>"><br>
	<textarea rows="5" cols="100" id="contenu" name="contenu" placeholder="Ecrire votre article ici"><?=$article['art_contenu']?></textarea>
	<input type="hidden" name="id" value="<?=$article['art_id']?>"><br>
	<input type="submit" value="Publier la modification">
</form>

<a href="index.php">Retour à l'accueil</a>

<?php
require_once 'include/foot.php';
?>