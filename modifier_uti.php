<?php
require_once 'include/verification.php';
require_once 'include/head.php';
require_once 'include/connexion_bdd.php';

if (!empty($_POST)) {
	$req = $pdo->prepare('
		UPDATE utilisateur
		SET uti_nom=:nom, uti_prenom=:prenom, uti_email=:email, uti_login=:login, uti_mdp=:mdp
		WHERE uti_id=:id
	');

	$req->execute([
		'nom'=> $_POST['nom'],
		'prenom'=> $_POST['prenom'],
		'email'=> $_POST['email'],
		'login'=> $_POST['login'],
		'mdp'=> $_POST['mdp'],
	]);

	header('location:index.php');

} else {
	$req = $pdo->prepare('
		SELECT * FROM utilisateur
		WHERE uti_id=:id
	');

	$req->execute([
		'id'=>$_GET['id']
	]);

	$user = $req->fetch(PDO::FETCH_ASSOC);
}

?>

<h1>Mettre à jour mon profil</h1>

<form action="modifier.php" method="post">
	<input type="text" id="nom" name="nom" value="<?=$user['uti_nom']?>"><br>
	<input type="text" id="prenom" name="prenom" value="<?=$user['uti_prenom']?>"><br>
	<input type="email" id="email" name="email" value="<?=$user['uti_email']?>"><br>
	<input type="text" id="login" name="login" value="<?=$user['uti_login']?>"><br>
	<input type="password" id="mdp" name="mdp"><br>
	<input type="hidden" name="id" value="<?=$user['uti_id']?>"><br>
	<input type="submit" value="Valider">
</form>

<a href="supprimer_uti.php?uti_id=<?=$user['uti_id']?>" onclick="return confirm('Êtes-vous certain de vouloir supprimer votre compte?')">Supprimer le compte</a>
<a href="index.php">Retour à l'accueil</a>

<?php
require_once 'include/foot.php';
?>