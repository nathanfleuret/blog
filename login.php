<?php
session_start();
require_once 'include/head.php';
require_once 'include/connexion_bdd.php';

if (!empty($_POST['formulaire4'])){
	$req = $pdo->prepare('
	SELECT * FROM utilisateur
	WHERE uti_login=:login
	');

	$req->execute([
		'login'=>$_POST['login']
	]);
	$user = $req->fetch(PDO::FETCH_ASSOC);

	$mdpCorrect= password_verify($_POST['mdp'],$user['uti_mdp']);
	
	if (!empty($user) && $mdpCorrect){
		$_SESSION['login']= $user['uti_login'];
		$_SESSION['id']= $user['uti_id'];
		header('location:index.php');

	} else {
		echo "<script>alert('Utilisateur ou mot de passe incorrect');</script>";
	}
}
?>

<h1>Connexion</h1>

<form action="login.php" method="post">
	<label for="login">Login</label>
	<input type="text" id="login" name="login" >
	<label for="login">Mot de passe</label>
	<input type="password" id="mdp" name="mdp" required>
	<input type="hidden" name="formulaire4" value="test">
	<input type="submit" value="Se connecter">
</form>
<a href="signin.php">Je n'ai pas encore de compte</a><br>
<a href="index.php">Retour à l'accueil</a>

<?php
require_once 'include/foot.php';
?>